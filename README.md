# Statistics Code Challenge



**Build & Execution**

Prerequisites:

1. JDK 1.8
2. Maven 3

This is a Spring Boot Application. You may execute it with the following commands:

1. cd statistics
2. mvn spring-boot:run

Important info:

1. application.properties file contains config parameters for Tomcat and H2 database (Tomcat port: 8089)
2. Under src\main\test\resources directory you may find same sample requests (to be imported in Postman) for the Statistics Rest API
3. The H2 console is reachable from this URL: http://localhost:8089/h2-console/
4. H2 info: Driver: org.h2.Driver JDBC URL: jdbc:h2:mem:testdb Username: sa Password: password

**Rest Endpoints**

The solution consists of two endpoints:

1. http://localhost:8089/xe/v1/AdService/ads/ (with optional parameters pageNum, pageSize and searchText) The default
   value for pageNum is 1 (the first page of results) and the default value for pageSize is 10 (the default value of entries
   per page). 
   This operation searches for the ads that fulfil the search criteria and returns the ads that fulfil also the pagination
   criteria. During the call of this operation a cookie with the search id is saved in order to be used by the 
   operation that returns a specific advertisment. An asynchronous service is also invoked in order to calculate and persist the 
   statistics. Two types of statistics are saved: One type concerns the ads that fulfil the search results(and are not
   displayed in the returned page) and the other one concerns the ads that fulfil the search results and are displayed in
   the results page.  
2. http://localhost:8089/xe/v1/AdService/ads/{id} 
   This operation returns a specific Ad (if exists), retrieves cookie information (with the searchId) and calls 
   an async service in order to save the statistics information.

**Proposed solution for processing the statistics data** 

The logic for the statistics solution is captured in the following points:

1. When the rest service is invoked, a service that handles the statistics is called asynchronously in order to avoid
   delay in the response of the rest service. This async service  performs calculations regarding the statistics and 
   the persistence of these statistics.
2. The statistics data are saved in two database tables:

    a. Table AD_SEARCH_STATISTIC holds statistics information regarding the ads that fulfilled a search. Each entry 
    contains the search id, search text, the uids of advertisements separated by a semicolon, a flag that states if the
    table entry has the uids of ads that appeared in the results page  and the page number (if applicable)
    
    b. Table AD_VISIT_STATISTIC holds statistics information regarding the visit of and advertisment. The uid of the 
    advertisment is stored along with the search id if applicable. (The search data are retrieved if 
    available through a cookie between the rest calls) According to several articles this info exchange between rest calls
    does not break the stateless nature and is acceptable. 
   These data will be processed by a job that will be started by a Scheduler. The job will be scheduled to run on a time slo
   when the system does not usually accept many requests (perhaps during the night) and preferably on a daily basis.
   The job could be scheduled to be executed more than one time each day - depending on the stress that is caused to the
   system along with the needs of the product owner.
   This job will retrieve the entries, make correlations of the search ids and the advertisment uids and finally perform
   an update on the visit numbers of the advertisements. We need to focus on having as much pre processing in order to avoid
   updating many times the same ads.
 

