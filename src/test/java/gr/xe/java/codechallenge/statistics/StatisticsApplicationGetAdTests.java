package gr.xe.java.codechallenge.statistics;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class StatisticsApplicationGetAdTests {

    private static final String SERVICE_URL
            = "http://localhost:8089/xe/v1/AdService/ads";

    @Test
    void assertGetAdStatus_OK() throws IOException {

        HttpUriRequest request = new HttpGet(SERVICE_URL + "/4");

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        Assertions.assertEquals(httpResponse
                .getStatusLine()
                .getStatusCode(), HttpStatus.SC_OK);
    }

    @Test
    void assertGetAdStatus_NOT_FOUND_pageNum() throws IOException {

        HttpUriRequest request = new HttpGet(SERVICE_URL + "/499d502a-a676-4471-9690-35714885356f");

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        Assertions.assertEquals(httpResponse
                .getStatusLine()
                .getStatusCode(), HttpStatus.SC_NOT_FOUND);
    }


}

