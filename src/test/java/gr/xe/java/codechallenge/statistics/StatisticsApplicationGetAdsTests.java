package gr.xe.java.codechallenge.statistics;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class StatisticsApplicationGetAdsTests {

    private static final String SERVICE_URL
            = "http://localhost:8089/xe/v1/AdService/ads";

    @Test
    void assertGetAdsStatus_OK() throws IOException {

        HttpUriRequest request = new HttpGet(SERVICE_URL);

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        Assertions.assertEquals(httpResponse
                .getStatusLine()
                .getStatusCode(), HttpStatus.SC_OK);
    }

    @Test
    void assertGetAdsStatus_NOT_FOUND_pageNum() throws IOException {

        HttpUriRequest request = new HttpGet(SERVICE_URL + "?pageNum=0");

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        Assertions.assertEquals(httpResponse
                .getStatusLine()
                .getStatusCode(), HttpStatus.SC_NOT_FOUND);
    }

    @Test
    void assertGetAdsStatus_NOT_FOUND_pageSize() throws IOException {

        HttpUriRequest request = new HttpGet(SERVICE_URL + "?pageSize=-1");

        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        Assertions.assertEquals(httpResponse
                .getStatusLine()
                .getStatusCode(), HttpStatus.SC_NOT_FOUND);
    }

}

