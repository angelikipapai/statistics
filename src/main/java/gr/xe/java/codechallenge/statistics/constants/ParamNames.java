package gr.xe.java.codechallenge.statistics.constants;

/**
 * Constants for Parameters names
 */
public final class ParamNames {
    public static final String PAGE_NUM = "pageNum";
    public static final String PAGE_SIZE = "pageSize";
    public static final String SEARCH_TEXT = "searchText";
    public static final String ID = "id";

}
