package gr.xe.java.codechallenge.statistics.constants;

/**
 * Constants for Cookies names
 */
public final class CookiesNames {
    public static final String XE_SEARCH_ID = "xeSearchId";

}
