package gr.xe.java.codechallenge.statistics.persistence;

import org.springframework.data.repository.CrudRepository;

public interface AdSearchStatisticRepository extends CrudRepository<AdSearchStatistic, Long> {
}
