package gr.xe.java.codechallenge.statistics.ad;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * Model Class of an Advertisement
 */
public class Ad implements Serializable {

    private static final long serialVersionUID = 1L;
    protected String id;
    protected String text;
    protected int customerId;
    protected Date createdAt;

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public int getCustomerId() {
        return customerId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Ad() {
        id = UUID.randomUUID().toString();
        text = "This is the ad text";

        Random rnd = new Random();

        customerId = Math.abs(rnd.nextInt(500));
        createdAt = Calendar.getInstance().getTime();
    }
}
