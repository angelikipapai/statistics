package gr.xe.java.codechallenge.statistics.ad;


import gr.xe.java.codechallenge.statistics.persistence.AdSearchStatistic;
import gr.xe.java.codechallenge.statistics.persistence.AdSearchStatisticRepository;
import gr.xe.java.codechallenge.statistics.persistence.AdVisitStatistic;
import gr.xe.java.codechallenge.statistics.persistence.AdVisitStatisticRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * This is an asynchronous Service that calculates and persists statistics for visits and display of Advertisements
 */
@Component
public class AdStatisticsAsyncService {
    Logger logger = LoggerFactory.getLogger(AdStatisticsAsyncService.class);
    @Autowired
    AdVisitStatisticRepository adVisitStatisticRepository;
    @Autowired
    AdSearchStatisticRepository adSearchStatisticRepository;

    /**
     * Persists two entries. One entry holds the uids (separated by a semicolon) of the ads that fulfil a search
     * but are not displayed in the requested page along with search info. The second entry holds the the uids
     * (separated by a semicolon) of the ads that fulfil a search and were returned in the requested page.
     *
     * @param searchResults the list with ads that fulfil the free text search
     * @param searchText    The free text search
     * @param searchId      the search id
     * @param startIndex    the start index of the results of the requested page
     * @param endIndex      the end index of the results of the requested page
     */
    @Async
    public void saveAdSearchStatistic(Ad[] searchResults, String searchText, String searchId, int startIndex, int endIndex, int pageNum) {
        logger.info("saveAdSearchStatistic: searchText={} searchId={} pageNum={} " , searchText + searchId + pageNum);
        AdSearchStatistic adSearchStatisticNotInPage = getAdSearchStatistic(searchText, searchId, false);
        AdSearchStatistic adSearchStatisticInPage = getAdSearchStatistic(searchText, searchId, true);
        adSearchStatisticInPage.setPageNumber(new Long(pageNum));


        StringBuffer advertisementIdsInPage = new StringBuffer();
        StringBuffer advertisementIdsNotInPage = new StringBuffer();

        //bypass the results that are displayed in the requested page because
        for (int i = 0; i < searchResults.length; i++) {
            if (i < startIndex || i > endIndex) {
                advertisementIdsNotInPage.append(searchResults[i].getId());
                advertisementIdsNotInPage.append(";");
            } else {
                advertisementIdsInPage.append(searchResults[i].getId());
                advertisementIdsInPage.append(";");
            }
        }
        if (advertisementIdsNotInPage.length() != 0 ) {
            advertisementIdsNotInPage.deleteCharAt(advertisementIdsInPage.length() - 1);
        }


        if ((advertisementIdsInPage.length() != 0)&& (advertisementIdsInPage.charAt(advertisementIdsInPage.length() - 1)==';')) {
            advertisementIdsInPage.deleteCharAt(advertisementIdsInPage.length() - 1);
        }
        if ((advertisementIdsNotInPage.length() != 0)&& (advertisementIdsNotInPage.charAt(advertisementIdsNotInPage.length() - 1)==';')) {
            advertisementIdsNotInPage.deleteCharAt(advertisementIdsNotInPage.length() - 1);
        }

        adSearchStatisticNotInPage.setAdvertisementIds(advertisementIdsNotInPage.toString());
        adSearchStatisticInPage.setAdvertisementIds(advertisementIdsInPage.toString());

        if (advertisementIdsNotInPage.length() != 0) {
            adSearchStatisticRepository.save(adSearchStatisticNotInPage);
        }
        if (advertisementIdsInPage.length() != 0) {
            adSearchStatisticRepository.save(adSearchStatisticInPage);
        }

    }

    private AdSearchStatistic getAdSearchStatistic(String searchText, String searchId, boolean b) {
        AdSearchStatistic adSearchStatistic = new AdSearchStatistic();
        adSearchStatistic.setSearchText(searchText);
        adSearchStatistic.setSearchId(searchId);
        adSearchStatistic.setCreationDateTime(new Date());
        adSearchStatistic.setAppearedInPage(b);
        return adSearchStatistic;
    }

    /**
     * Persists an entry with the uid of an Ad that was displayed along with related
     * search id if applicable
     *
     * @param ad       An advertisment that was displayed
     * @param searchId
     */
    @Async
    public void saveAdVisitStatistic(Ad ad, String searchId) {
        logger.info("saveAdVisitStatistic: searchId={} " , searchId);
        AdVisitStatistic adVisitStatistic = new AdVisitStatistic();
        adVisitStatistic.setAdvertismentId(ad.getId());
        adVisitStatistic.setCreationDateTime(new Date());
        if (searchId != null) {
            adVisitStatistic.setSearchId(searchId);
        }
        adVisitStatisticRepository.save(adVisitStatistic);
    }


}
