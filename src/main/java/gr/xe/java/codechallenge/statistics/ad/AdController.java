package gr.xe.java.codechallenge.statistics.ad;

import gr.xe.java.codechallenge.statistics.exception.NotFoundResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import static gr.xe.java.codechallenge.statistics.constants.CookiesNames.XE_SEARCH_ID;
import static gr.xe.java.codechallenge.statistics.constants.ParamNames.*;

@Component
@Path("/v1/AdService")
public class AdController {

    Logger logger = LoggerFactory.getLogger(AdController.class);

    @Autowired
    AdService adService;

    /**
     * @param pageNum    the number of the requested page (default value is 1)
     * @param pageSize   the number of results per page (default value is 10)
     * @param searchText the free text used for search
     * @return the results that are applicable based on search and pagination criteria
     * along with pagination info
     */
    @GET
    @Path("/ads")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAds(@DefaultValue("1") @QueryParam(PAGE_NUM) int pageNum, @DefaultValue("10") @QueryParam(PAGE_SIZE) int pageSize, @QueryParam(SEARCH_TEXT) String searchText) {
        try {
            logger.info("getAds: searchText={} pageNum={} pageSize={} ", searchText, pageNum, pageSize);
            AdPage ads = adService.getAds(pageNum, pageSize, searchText);
            NewCookie cookie = new NewCookie(XE_SEARCH_ID, ads.getSearchId());

            Response.ResponseBuilder builder = Response.status(Response.Status.OK).cookie(cookie);
            builder.entity(ads);
            return builder.build();
        } catch (NotFoundResourceException e) {
            throw new NotFoundException();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new InternalServerErrorException();
        }

    }

    /**
     * @param id         the id of the requested Ad
     * @param xeSearchId Cookie with the search text id
     * @return and Ad that has the requested id
     */
    @GET
    @Path("/ads/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAd(@PathParam(ID) String id, @CookieParam(XE_SEARCH_ID) String xeSearchId) {
        try {
            logger.info("getAd: id={} xeSearchId={} ", id, xeSearchId);
            Response.ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            builder.status(Response.Status.OK).entity(adService.getAd(id, xeSearchId));
            return builder.build();
        } catch (NotFoundResourceException e) {
            throw new NotFoundException();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new InternalServerErrorException();
        }

    }


}

