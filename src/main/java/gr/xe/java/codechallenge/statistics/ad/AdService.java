package gr.xe.java.codechallenge.statistics.ad;

import gr.xe.java.codechallenge.statistics.exception.NotFoundResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for Advertisements
 */
@Service
public class AdService {

    Logger logger = LoggerFactory.getLogger(AdService.class);
    @Autowired
    AdStatisticsAsyncService statisticsAsyncService;

    /**
     * This methods performs the search and the pagination.
     * and  call to async service for saving statistics.
     *
     * @param pageNum    the number of the requested page
     * @param pageSize   the number of results per page
     * @param searchText the free text used for search
     * @return a Page with results that are applicable based on search and pagination criteria
     * @throws NotFoundResourceException
     */
    public AdPage getAds(int pageNum, int pageSize, String searchText) throws NotFoundResourceException {
        logger.info("getAds: searchText={} pageNum={} pageSize={} ", searchText, pageNum, pageSize);
        if (pageSize < 1 || pageNum < 1) {
            throw new NotFoundResourceException();
        }
        //Get all applicable data
        AdSearch search = new AdSearch(searchText);
        final Ad[] searchResults = search.getSearchResults();
        //Calculate pagination info
        int totalResults = searchResults.length;
        int totalPages = totalResults / pageSize;
        int pagesRemainder = totalResults % pageSize;
        if (pagesRemainder > 0) {
            totalPages++;
        }
        if (pageNum > totalPages) {
            throw new NotFoundResourceException();
        }
        Ad[] pageSearchResults;
        if (pageNum == totalPages && pagesRemainder != 0) {
            pageSearchResults = new Ad[pagesRemainder];
        } else {
            pageSearchResults = new Ad[pageSize];
        }
        //Filter the paginated results
        int startIndex = pageNum * pageSize - pageSize;
        int endIndex = pageNum * pageSize - 1;
        if (pageNum==totalPages && pagesRemainder!=0 ){
            endIndex= startIndex + pagesRemainder -1;
        }
        int j = 0;
        for (int i = startIndex; i <= endIndex; i++) {
            pageSearchResults[j] = searchResults[i];
            j++;
        }

        AdPage adPage = new AdPage(searchResults.length, pageNum, pageSize);
        adPage.setSearchResults(pageSearchResults);
        adPage.setSearchId(search.searchId);
        //call async service to save statics for ads
        statisticsAsyncService.saveAdSearchStatistic(searchResults, searchText, search.searchId, startIndex, endIndex, pageNum);
        return adPage;
    }

    /**
     * This method returns the request ad and performs call to the async service for saving statistics
     *
     * @param id         The id of the requested Ad
     * @param xeSearchId the value of the Cookie for search Id
     * @return an Advertisement
     * @throws Exception
     */
    public Ad getAd(String id, String xeSearchId) throws Exception {
        logger.info("getAd: id={} xeSearchId={} ", id, xeSearchId);
        //we model here a case when the resource does not exist
        if (id.equals("499d502a-a676-4471-9690-35714885356f")) {
            throw new NotFoundResourceException();
        }
        //we model here a case when the resource is found
        Ad ad = new Ad();
        //call async service to save statistic for Ad view
        statisticsAsyncService.saveAdVisitStatistic(ad, xeSearchId);
        return ad;
    }
}
