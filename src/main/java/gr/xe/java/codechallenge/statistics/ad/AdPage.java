package gr.xe.java.codechallenge.statistics.ad;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Model Class of a Page with Advertisements
 */
public class AdPage implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String searchId;
    private int totalResults;
    private int pageNum;
    private int pageSize;
    private Ad[] searchResults;

    public AdPage(int totalResults, int pageNum, int pageSize) {
        this.totalResults = totalResults;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Ad[] getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(Ad[] searchResults) {
        this.searchResults = searchResults;
    }

}
