package gr.xe.java.codechallenge.statistics.persistence;


import javax.persistence.*;
import java.util.Date;

/**
 * Entity for AD_VISIT_STATISTIC
 */
@Entity
public class AdVisitStatistic {

    @Column(name = "CREATION_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date creationDateTime;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "SEARCH_ID")
    private String searchId;
    @Column(name = "ADVERTISMENT_ID", columnDefinition = "CLOB NOT NULL")
    private String advertismentId;

    public AdVisitStatistic() {
    }

    public Long getId() {
        return id;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public String getAdvertismentId() {
        return advertismentId;
    }

    public void setAdvertismentId(String advertismentId) {
        this.advertismentId = advertismentId;
    }

    public Date getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

}
