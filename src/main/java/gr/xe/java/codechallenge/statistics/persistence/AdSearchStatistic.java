package gr.xe.java.codechallenge.statistics.persistence;

import javax.persistence.*;
import java.util.Date;

/**
 * Entity for AD_SEARCH_STATISTIC
 */
@Entity
public class AdSearchStatistic {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "SEARCH_TEXT")
    private String searchText;
    @Column(name = "SEARCH_ID")
    private String searchId;
    @Column(name = "ADVERTISMENT_IDS", columnDefinition = "CLOB NOT NULL")
    @Lob
    private String advertisementIds;
    @Column(name = "APPEARED_IN_PAGE")
    private boolean appearedInPage;
    @Column(name = "PAGE_NUMBER")
    private Long pageNumber;
    @Column(name = "CREATION_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    Date creationDateTime;


    public AdSearchStatistic() {
    }

    public Long getId() {
        return id;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public String getAdvertisementIds() {
        return advertisementIds;
    }

    public void setAdvertisementIds(String advertisementIds) {
        this.advertisementIds = advertisementIds;
    }

    public boolean isAppearedInPage() {
        return appearedInPage;
    }

    public void setAppearedInPage(boolean appearedInPage) {
        this.appearedInPage = appearedInPage;
    }

    public Long getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Long pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Date getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
}
