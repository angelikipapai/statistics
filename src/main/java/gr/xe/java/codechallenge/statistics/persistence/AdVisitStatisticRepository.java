package gr.xe.java.codechallenge.statistics.persistence;

import org.springframework.data.repository.CrudRepository;

public interface AdVisitStatisticRepository extends CrudRepository<AdVisitStatistic, Long> {
}
