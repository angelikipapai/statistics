package gr.xe.java.codechallenge.statistics.config;

import gr.xe.java.codechallenge.statistics.ad.AdController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

/**
 * This Class contains some configurations needed in order to use JAX-RS (Jersey implementation) instead of Spring MVC
 */
@Component
@ApplicationPath("xe")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        registerEndpoints();
    }

    private void registerEndpoints() {
        register(AdController.class);
    }
}
